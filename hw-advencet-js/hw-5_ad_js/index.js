

class Card {
    constructor(name, email, title, text,id) {
        this.name = name,
            this.email = email,
            this.title = title,
            this.text = text,
            this.id = id
            
    }

    createElement() {
        const render = document.querySelector('.card')
        render.insertAdjacentHTML('afterbegin',
            `<div class="card-container" data-id="${this.id}">
        <div class="avatar"><img class="user-img" src="./img/pngwing.com.png" alt=""> </div>
        <div class="card-info">
            <p><i class="fa-brands fa-twitter"></i> ${this.name}</p>
            <a href="" class="emeil"> ${this.email}</a>
            <h1 class="card-title">${this.title}</h1>
            <p class="card-text">${this.text}</p>
            <div class="card-icons">
                <i class="fa-regular fa-comment"> <span class="icon-count">25</span></i>
                <i class="fa-solid fa-retweet"> <span class="icon-count">5тис.</span></i>
                <i class="fa-regular fa-heart"> <span class="icon-count">36</span></i>
                <i class="fa-solid fa-eye"> <span class="icon-count">20тис.</span></i>
                <i class="fa-solid fa-arrow-up-from-bracket"></i>
            </div>
        </div>
        <button class="card-btn"onclick="deletePost(${this.id})" >Delete</button>
    </div> `)
    }
}


fetch('https://ajax.test-danit.com/api/json/posts')
    .then(res => res.json())
    .then(posts => {
        console.log(posts)
        fetch('https://ajax.test-danit.com/api/json/users')
            .then(res => res.json())
            .then(users => {
                console.log(users)
                posts.forEach(post => {

                    const { name, email,id } = users.find(el => el.id === post.userId)
                
                    new Card(name, email, post.title, post.body,id ).createElement();
                })


            }).catch(err => console.log(err))


    }).catch(err => console.log(err))



    const deletePost = (id) => {

        fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
          method: "DELETE",
        })
            .then(response => {console.log(response)
        
        
            let container = document.querySelector(`[data-id='${id}']`);
            container.remove();
        
          })
          .catch((error) => {
            console.error("Помилка видалення", error);
          });
    };









