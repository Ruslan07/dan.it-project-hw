


class Location {
    constructor(timezone, country, regionName, city,zip) {
        this.timezone = timezone,
            this.country = country,
            this.regionName = regionName,
            this.city = city,
            this.zip = zip
    }

    render() {
        const info = document.getElementById('info');
        info.insertAdjacentHTML('afterbegin', `<ul>
    <li>Континент: ${this.timezone}</li>
     <li>Країна: ${this.country}</li>
     <li>Регіон: ${this.regionName}</li>
     <li>Місто: ${this.city}</li>
     <li>Район: ${this.zip}</li>
     </ul>`

        )
    }




}



const getIp = async () => {
    try{
        const respons = await fetch('https://api.ipify.org/?format=json');
    const res = await respons.json()
    return res.ip 
    }
    catch(err){console.log(err)

    }
   
}
console.log(getIp())

const getInfo = async () => {

    try{
          let result = await getIp();
    const getReguest = await fetch(`http://ip-api.com/json/${result}`);
    const res = getReguest.json();
    const {timezone, country, regionName, city, zip} = await res
    new Location(timezone,country, regionName, city,zip).render()
    
    }
  catch(err){
    console.log(err)
  }
}

document.querySelector('.btn').addEventListener('click',getInfo)