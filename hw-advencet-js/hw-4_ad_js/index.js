/*Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
Це технологія, що дозволяє підвантажувати ряд даних на веб-сторінці без перевантаження самої сторінки, що, 
відповідно, зменшить кількість запитів до сервера, а також об’єм підвантажуваних даних.
В основному технологія використовується для підвантаження окремих даних, відправки даних форм, 
а саме авторизація, добавлення коментарів чи відправки повідомлень.
*/

const loader = document.querySelector('.loader')

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(res => res.json())
    .then(data => {
        console.log(data)
        data.forEach(({ episodeId, name, openingCrawl, characters }) => {
            const data = new Data(episodeId, name, openingCrawl, characters);
            data.render();
            data.printCharacters();
            loader.style.display = 'none'
        })

    })
    .catch(error => { console.log(error) });



class Data {
    constructor(episodeId, name, openingCrawl, characters) {
        this.episodeId = episodeId;
        this.name = name;
        this.openingCrawl = openingCrawl;
        this.characters = characters
        this.container = document.createElement('div');

    }
    printCharacters() {
        this.characters.forEach(url => {
            console.log(url)
            fetch(url).then(res => res.json())
                .then(({ name }) => {
                    const ul = this.container.querySelector('.characters-list');
                    ul.innerHTML += `<li>${name} </li>`;
                }
                )

        })
    }




    render() {

        this.container.innerHTML = `
<h2>Episode ${this.episodeId}: ${this.name}</h2>
<ul class="characters-list"></ul>
<p>${this.openingCrawl}</p>`;
        document.body.append(this.container);
    }

}


