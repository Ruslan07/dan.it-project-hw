/*Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
коли потрібно щоб код виконувався незважаючи на те що є помилка,або згенерувати помилку(виключення) самостійно

*/



const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

class Item {
  constructor(book) {
   
    if (typeof book.author === "undefined" || typeof book.name === "undefined" || typeof book.price === "undefined") {
      throw console.error(`Помилка`);
     
    }
    this.book = book;
  }
  render() {
    const root = document.getElementById("root");
    const ul = document.createElement("ul");
    const li = document.createElement("li");
    li.innerHTML = `Автор: ${this.book.author}<br>Назва: ${this.book.name}<br>Ціна: ${this.book.price}`;
    ul.append(li);
    root.append(ul);
  }
}

books.forEach((book) => {
  // console.log(book)
  try {
    new Item(book).render();
  } catch (err) {
 
      console.error(`Помилка: в об'єкті відсутня одна з властивостей: ${JSON.stringify(book)}`);


    
  }

});



