/*
Теоретичне питання
1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
 Кожен Дочірній об'єкт має внутрішнє посилання на батьківський об'єкт - його прототип. Коли властивість
  запитується з об'єкту  і якщо вона не знайдена в самому об'єкті, JavaScript продовжить 
  пошук в його прототипі, поки властивість не буде знайдена або  досягнено кінця ланцюжка прототипів.



2.Для чого потрібно викликати super() у конструкторі класу-нащадка?
  super() викликається для того щоб успадкувати  властивості батьківського класу. 
  У super() потрібно передати  аргументи,  що отримує конструктор класу-нащадка. 

*/








class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get nevName() {
        return this.name;
    };
    set nevName(value) {
        return this.name = value
    };
    get nevAge() {
        return this.age;
    };
    set nevAge(value) {
        return this.age = value
    };
    get nevSalary() {
        return this.salary;
    };
    set nevSalary(value) {
        return this.salary = value
    };
}

class Programmer extends Employee {
    constructor(name,age,salary,lang){
        super(name,age,salary);
        this.lang = lang;

    }
    get nevSalary() {
        return this.salary*3;
    };
}
const newProgrammer = new Programmer( 'Ruslan', 32, 1000, 'js')
console.log(newProgrammer);
console.log(newProgrammer.nevSalary)

const newProgrammer2 = new Programmer( 'Ivanka', 32, 2000, 'Python')
console.log(newProgrammer2);
console.log(newProgrammer2.nevSalary)

const newProgrammer3 = new Programmer( 'Lev', 21, 3000, 'Html')
console.log(newProgrammer3);
console.log(newProgrammer3.nevSalary)




