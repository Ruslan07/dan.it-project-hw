import imagemin from 'gulp-imagemin';
import gulp from 'gulp';
// import minify from'gulp-minify';
import htmlmin from 'gulp-htmlmin';
import concat from 'gulp-concat'
import terser from 'gulp-terser';
import cleanCSS from 'gulp-clean-css';
// import GulpClient from'gulp';
import clean from 'gulp-clean';
import bs from 'browser-sync';

const browserSync = bs.create();
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);

const scss = () => {
    return gulp.src('./src/scss/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
  };



const html =()=>{
    return gulp.src('./src/**/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('./dist'))
}

const imgMin = () =>{
    return gulp.src('./src/images/**')
    .pipe(imagemin()).pipe(gulp.dest('dist/images'))
}


const js = () =>{
    return gulp.src('./src/**/*.js')
    .pipe(concat('all.js'))
    .pipe(terser())
    .pipe(gulp.dest('./dist/js'))
}
const cleanDist =() =>{
    return gulp.src('./dist', {read: false})
    .pipe(clean());
}
const css = ()=>{
    return gulp.src('./dist/css/*.css')
    .pipe(concat('all.css'))
    .pipe(cleanCSS())
    .pipe(gulp.dest('./dist/css')) 
} 


const dev =()=>{
    
        browserSync.init({
            server: {
                baseDir: "./dist"
            }
        });
    gulp.watch('./src/**/*',gulp.series(cleanDist,gulp.parallel(html,imgMin, js,css, scss),(next) => {
        browserSync.reload();
    next()
    }))
}
// function buildStyles() {
//     return gulp.src('./src/scss/**/.scss')
//       .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
//       .pipe(gulp.dest('./css'));
//   };

gulp.task('img',imgMin)
gulp.task('html',html);
gulp.task('js',js);
gulp.task('css',css)
gulp.task('scss',scss)
gulp.task('clean',cleanDist)
gulp.task('html',html);
gulp.task('build',gulp.series(cleanDist,gulp.parallel(html, js,imgMin,scss, css, )))
gulp.task('dev',gulp.series('build',dev))












