import React, { useState } from 'react';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';
import './App.scss';



const App = ( ) => {
  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [secondModalOpen, setSecondModalOpen] = useState(false);


  const openFirstModal = () => {
    setFirstModalOpen((prev)=>{
      return !prev;
    })

  };
 
  const openSecondModal = () => {
    setSecondModalOpen((prev)=>{
      return !prev;
    });
  };
  const closeModal = () => {
    return setFirstModalOpen(),
    setSecondModalOpen()
  };

 const  fonnClick = (event) => {
    // Перевіряємо, чи клік був здійснений за межами модального вікна
    if (event.currentTarget === event.target) {
      //Якщо так, то додаємо код для закриття модального вікна
      setFirstModalOpen()
      setSecondModalOpen()
    }
  };




  return (
    <div  >
      <Button

        backgroundColor=" aqua"
        text="Open first modal"
        onClick={openFirstModal}
      />
      <Button

        backgroundColor="chartreuse"
        text="Open second modal"
        onClick={openSecondModal}
      />

      {firstModalOpen && (
        <Modal
          header="Do you want to delete this file?"
          closeButton={true}
          closeModal={closeModal}
          fonnClick={fonnClick}
          text="Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
          actions={
            <div >
              <Button
                backgroundColor="yellow"
                onClick={closeModal}
                text="Ok"
              />
              <Button
                backgroundColor="mediumorchid"
                onClick={closeModal}
                text="Not"
              />
            </div>
          }
        />
      )}

      {secondModalOpen && (
        <Modal
          header="Home Work 1 - MODAL-WINDOW "
          closeButton={true}
          closeModal={closeModal}
          fonnClick={fonnClick}
          text="How would you rate my work?"
          actions={
            <div >
              <Button

                backgroundColor="yellow"
                onClick={closeModal}
                text="Ok"
              />

              <Button

                backgroundColor="mediumorchid"
                onClick={closeModal}
                text="Next"
              />

            </div>
          }
        />
      )}
    </div>
  )
}

export default App;

