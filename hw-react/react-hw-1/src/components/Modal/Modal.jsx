import React from "react";
import styles from "./Modal.module.scss";

const Modal=(props)=>{
        const { header, closeButton, text, actions,closeModal, fonnClick } = props;



    return(
            <div className={styles.modalWrapper} onClick={fonnClick}>
                <div className={styles.modal}>
                    <div className={styles.modalContent}>
                        <div className={styles.modalHeader}>
                            <h2>{header}</h2>
                            {closeButton && (
                                <span className={styles.modalClose} onClick={closeModal} >
                                    &times;
                                </span>
                            )}
                        </div>
                        <div >
                            <p className={styles.bodyText}>{text}</p>
                        </div>
                        <div >{actions}</div>
                    </div>
                </div>
            </div>
    )
}

export default Modal;

// export default class Modal extends React.Component {

//     render() {
//         const { header, closeButton, text, actions,closeModal, fonnClick } = this.props;

//         return (
//             <div className={styles.modalWrapper} onClick={fonnClick}>
//                 <div className={styles.modal}>
//                     <div className={styles.modalContent}>
//                         <div className={styles.modalHeader}>
//                             <h2>{header}</h2>
//                             {closeButton && (
//                                 <span className={styles.modalClose} onClick={closeModal} >
//                                     &times;
//                                 </span>
//                             )}
//                         </div>
//                         <div >
//                             <p className={styles.bodyText}>{text}</p>
//                         </div>
//                         <div >{actions}</div>
//                     </div>
//                 </div>
//             </div>
//         )


//     }



// }







