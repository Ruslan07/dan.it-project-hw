import React from "react";
import styles from "./Button.module.scss";

const Button = (props) => {
    const { backgroundColor, text, onClick, } =props;


    return(
        <button
            className={styles.btn}
            style={{ backgroundColor }}
            onClick={onClick}
            
        >

           {text}
        </button>
    )
};

export default Button;

// export default class Button extends React.Component{
// render(){


// const { backgroundColor, text, onClick } = this.props;
//     return (
//         <button
//             className={styles.btn}
//             style={{ backgroundColor }}
//             onClick={onClick}
//         >

//            {text}
//         </button>
//     )}

// }
    



