import React, { useState, useEffect } from "react";
import Header from "./components/Header/Header";
import styles from "./App.module.scss";
import ItemsContainer from "./components/ItemsContainer/ItemsContainer";
import { getStateFromLocalStorage,saveStateToLocalStorage } from "./components/utils/localStorageHelper";
import { CART_LS_KEY, FAV_LS_KEY } from "./components/constants";
import Button from "./components/Button/Button";
import ModalFavorite from "./components/ModalFavorit/ModalFavorit";

function App() {
  const [items, setitems] = useState([]);
  const [cartItems, setCartItems] = useState([]); //cart = корзина, card = карточка
  const [favorites, setFavorites] = useState([]);
  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [currentProduct, setCurrentProduct] = useState(null);

  console.log(currentProduct);

  const toggleModal = () => {
    setFirstModalOpen((prev) => !prev);
    if (firstModalOpen) setCurrentProduct({})
  };

  const fonnClick = (event) => {
    // Перевіряємо, чи клік був здійснений за межами модального вікна
    if (event.currentTarget === event.target) {
      //Якщо так, то додаємо код для закриття модального вікна
      toggleModal();
    }
  };

  const handleAddToCart = () => {
    const { name, price, id } = currentProduct
    setCartItems((prev) => {
      const newCartState = [...prev];
      const index = newCartState.findIndex((item) => item.id === id);
      if (index !== -1) {
        newCartState[index].count++;
        saveStateToLocalStorage(CART_LS_KEY, newCartState);
        return newCartState;
      } else {
        const newState = [{ name, price, id, count: 1 }, ...prev];
        saveStateToLocalStorage(CART_LS_KEY, newState);

        return newState;
      }
    });
    toggleModal();
  };

  const getData = async () => {
    try {
      const newItem = await fetch("./products.json").then((res) => res.json());
      // console.log(newItem.data);
      setitems(newItem);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    getData();
    const favFromLs = getStateFromLocalStorage(FAV_LS_KEY);
    if (favFromLs) {
      setFavorites(favFromLs);
    }
    const cartFromLs = getStateFromLocalStorage(CART_LS_KEY);
    if (cartFromLs) {
      setCartItems(cartFromLs);
    }
  }, []);

  return (
    <div className={styles.root}>
      <div className={styles.container}>
        <Header cartItems={cartItems} favorites={favorites} />
      </div>
      {items && (
        <ItemsContainer
          items={items}
          favorites={favorites}
          setFavorites={setFavorites}
          setCurrentProduct={setCurrentProduct}
          toggleModal={toggleModal}
        />
      )}
      {firstModalOpen && (
        <ModalFavorite
          header="Бажаєте добавити товар в Кошик"
          closeButton={true}
          closeModal={toggleModal}
          fonnClick={fonnClick}
          text="Товар"
          actions={
            <div>
              {currentProduct.name} - {currentProduct.price} грн.
              <Button
                className={styles.btn}
                onClick={handleAddToCart}
                text="Додати в кошик"
              />
            </div>
          }
        />
      )}
    </div>
  );
}

export default App;
