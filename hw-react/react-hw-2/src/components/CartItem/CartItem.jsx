import React from "react";
import styles from "./CartItem.module.scss";
import PropTypes from "prop-types";

const CartItem = (props) => {
  const { name, price, id, count } = props;

  return (
    <div className={styles}>
      <div>
        <span>
          {name}
          {price}
          {id}
        </span>
      </div>
      <span>{count}</span>
    </div>
  );
};
CartItem.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  id: PropTypes.number,
  count: PropTypes.number,
};
export default CartItem;
