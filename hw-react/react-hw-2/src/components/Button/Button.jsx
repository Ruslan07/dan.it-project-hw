import React from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";
const Button = (props) => {
  const { backgroundColor, text, onClick } = props;

  return (
    <button
      className={styles.btn}
      style={{ backgroundColor }}
      onClick={onClick}
    >
      {text}
    </button>
  );
};
Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  type: "button",
  onClick: () => {},
};
export default Button;
