import React from "react";
import { getStateFromLocalStorage } from "../../components/utils/localStorageHelper";
import { CART_LS_KEY } from "../../components/constants";
import Item from "../../components/Item/Item";
import styles from "../../components/ItemsContainer/ItemsContainer.module.scss";
import ModalFavorite from "../../components/ModalFavorit/ModalFavorit";
import Button from "../../components/Button/Button";
const CartPage = ({
  cartItems,
  remuveAddToCart,
  toggleModal,
  setCurrentProduct,
  setCartItems,
  firstModalOpen,
  fonnClick,
  currentProduct,
}) => {
  console.log(cartItems);

  return (
    // console.log(cartItems)
    <div className="container">
      <div className={styles.productList}>
        {cartItems &&
          cartItems.map(({ id, name, price, image, count}) => (
            <Item
              key={id}
              name={name}
              price={price}
              image={image}
              id={id}
              count={count}
             
              setCartItems={setCartItems}
              cartItems={cartItems}
              setCurrentProduct={setCurrentProduct}
              toggleModal={toggleModal}
              setBtnTitle = {"Видалити"}
            />
          ))}
      </div>
      {firstModalOpen && (
        <ModalFavorite
          header="Бажаєте видалити товар"
          closeButton={true}
          closeModal={toggleModal}
          fonnClick={fonnClick}
          text="Товар"
          actions={
            <div>
              {currentProduct.name} - {currentProduct.price} грн.
              <Button
                className={styles.btn}
                onClick={remuveAddToCart}
                text="Видалити"
              />
            </div>
          }
        />
      )}
    </div>
  );
};
export default CartPage;
