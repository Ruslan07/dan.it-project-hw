import React from "react";
import Header from "../../components/Header/Header";
import styles from "../../components/Header/Header.module.scss";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";
import ModalFavorite from "../../components/ModalFavorit/ModalFavorit";
import Button from "../../components/Button/Button";
const HomePage = ({
  // cartItems,
  favorites,
  handleAddToCart,
  currentProduct,
  firstModalOpen,
  fonnClick,
  items,
  setFavorites,
  setCurrentProduct,
  toggleModal,
}) => {
  return (
    <div>
      {items && (
        <ItemsContainer
          items={items}
          favorites={favorites}
          setFavorites={setFavorites}
          setCurrentProduct={setCurrentProduct}
          toggleModal={toggleModal}
        />
      )}
      {firstModalOpen && (
        <ModalFavorite
          header="Бажаєте добавити товар в Кошик"
          closeButton={true}
          closeModal={toggleModal}
          fonnClick={fonnClick}
          text="Товар"
          actions={
            <div>
              {currentProduct.name} - {currentProduct.price} грн.
              <Button
                className={styles.btn}
                onClick={handleAddToCart}
                text="Додати в кошик"
              />
            </div>
          }
        />
      )}
    </div>
  );
};
export default HomePage;
