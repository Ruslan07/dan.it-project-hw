import React from "react";
import { getStateFromLocalStorage } from "../../components/utils/localStorageHelper";
import { FAV_LS_KEY } from "../../components/constants";
import Item from "../../components/Item/Item";
import styles from "../../components/ItemsContainer/ItemsContainer.module.scss";
import Button from "../../components/Button/Button";
import ModalFavorite from "../../components/ModalFavorit/ModalFavorit";
const FavoritsPage = ({
  favorites,
  setFavorites,
  setCurrentProduct,
  toggleModal,
  fonnClick,
  currentProduct,
  handleAddToCart,
  firstModalOpen,
  
}) => {
  
  return (
    <div className="container">
      <div className={styles.productList}>
        {favorites &&
          favorites.map(({ id, name, price, image }) => (
            <Item
              key={id}
              name={name}
              price={price}
              image={image}
              id={id}
              setFavorites={setFavorites}
              favorites={favorites}
              setCurrentProduct={setCurrentProduct}
              toggleModal={toggleModal}
              setBtnTitle="Додати в кошик"
            />
          ))}
      </div>
      {firstModalOpen && (
        <ModalFavorite
          header="Бажаєте товар в Кошик"
          closeButton={true}
          closeModal={toggleModal}
          fonnClick={fonnClick}
          text="Товар"
          actions={
            <div>
              {currentProduct.name} - {currentProduct.price} грн.
              <Button
                className={styles.btn}
                onClick={handleAddToCart}
                text="Додати"
              />
            </div>
          }
        />
      )}
    </div>
  );
};
export default FavoritsPage;
