import React from "react";
import { useEffect, useState } from "react";
import { CART_LS_KEY, FAV_LS_KEY } from "../constants";

import { saveStateToLocalStorage } from "../utils/localStorageHelper";
import { ReactComponent as StarIcon } from "./svg/StarIcon.svg";
import { ReactComponent as StarIconOutline } from "./svg/StarIconOutline.svg";
import styles from "./Item.module.scss";
import PropTypes from "prop-types";

const Item = (props) => {
  const {
    id,
    name,
    price,
    image,
    count,
    setFavorites,
    favorites,
    setCurrentProduct,
    toggleModal,
    setBtnTitle,
  } = props;

  const [isFavorited, setIsFavorited] = useState(false);

  useEffect(() => {
    const index = favorites?.findIndex((item) => item.id === id);
    if (index !== -1) {
      setIsFavorited(true);
    }
  }, []);

  const handleAddToCart = () => {
    setCurrentProduct({ id, name, price, image });
    toggleModal();
  };

  const handleAddToFavorites = () => {
    let newArray;
    setIsFavorited((prev) => !prev);

    setFavorites((prev) => {
      // const newCartState = [...prev];
      const index = prev.findIndex((item) => item.id === id);

      if (index !== -1) {
        newArray = prev.filter((item) => item.id !== id);
      } else {
        newArray = [...prev, { id, name, price, image }];
      }
      saveStateToLocalStorage(FAV_LS_KEY, newArray);
      return newArray;
    });
  };

  return (
    <div className={styles.productCard}>
      <img className={styles.imageWrapper} src={`./img/${image}`} alt="" />

      <h3 className={styles.productName}>{name}</h3>
      <p className={styles.count}> Кількість {count}</p>

      <div className={styles.cardFooter}>
        <button className={styles.addFavorite} onClick={handleAddToFavorites}>
          {isFavorited ? <StarIconOutline /> : <StarIcon />}
        </button>

        <p className={styles.productPrice}>{price} грн</p>
      </div>

      <button className={styles.addtoCart} onClick={handleAddToCart}>
        {setBtnTitle}
      </button>
    </div>
  );
};
Item.defaultProps = {
  setFavorites: () => {},
  setCurrentProduct: () => {},
  toggleModal: () => {},
};

Item.propTypec = {
  setFavorites: PropTypes.func,
  setCurrentProduct: PropTypes.func,
  toggleModal: PropTypes.func,
};

export default Item;
