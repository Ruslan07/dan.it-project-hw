import React from "react";
import Item from "../Item/Item";
import PropTypes from 'prop-types';
import styles from "./ItemsContainer.module.scss";

const ItemsContainer = (props) => {
  const { items, setFavorites, favorites, setCurrentProduct, toggleModal,text } = props;
 

  return (
    <div className="container">
      <div className={styles.productList}>
        {items &&
          items.map(({ id, name, price, image,setBtnTitle }) => (
            <Item
              key={id}
              name={name}
              price={price}
              image={image}
              id={id}
          
              setFavorites={setFavorites}
              favorites={favorites}
              setCurrentProduct={setCurrentProduct}
              toggleModal={toggleModal}
              setBtnTitle ='Додати до кошика'
            />
          ))}
      </div>
    </div>
  );
};
ItemsContainer.defaultProps={
  items:[],

}

ItemsContainer.propTypec={
  items:PropTypes.array,

}
export default ItemsContainer;
