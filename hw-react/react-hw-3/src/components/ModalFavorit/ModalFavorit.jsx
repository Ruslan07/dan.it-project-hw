import React from "react";
import styles from "./ModalFavorit.module.scss";
import PropTypes from 'prop-types';

const ModalFavorite = (props) => {
  const { header, closeButton, text, actions, closeModal, fonnClick } = props;

  return (
    <div className={styles.modalWrapper} onClick={fonnClick}>
      <div className={styles.modal}>
        <div className={styles.modalContent}>
          <div className={styles.modalHeader}>
            <h2>{header}</h2>
            {closeButton && (
              <span className={styles.modalClose} onClick={closeModal}>
                &times;
              </span>
            )}
          </div>
          <div>
            <p className={styles.bodyText}>{text}</p>
            <span></span>
          </div>
          <div className={styles.modalActions}>{actions}</div>
        </div>
      </div>
    </div>
  );
};
ModalFavorite.defaultProps={
    header:[],
    closeButton:true,
    fonnClick :()=>{},
  }
  
  ModalFavorite.propTypec={
    header:PropTypes.string,
    closeButton:PropTypes.bool,
    text:PropTypes.string,
    actions:PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.func]),
    closeModal:PropTypes.bool,
    fonnClick:PropTypes.func,
    

    
  }
export default ModalFavorite;
