import React, { useState, useEffect } from "react";
import Header from "./components/Header/Header";
import styles from "./App.module.scss";

import {
  getStateFromLocalStorage,
  saveStateToLocalStorage,
} from "./components/utils/localStorageHelper";
import { CART_LS_KEY, FAV_LS_KEY } from "./components/constants";

import AppRoutes from "./AppRoutes";

function App() {
  const[btnTitle, setBtnTitle]=useState()
  const [items, setitems] = useState([]);
  const [cartItems, setCartItems] = useState([]); //cart = корзина, card = карточка
  const [favorites, setFavorites] = useState([]);
  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [currentProduct, setCurrentProduct] = useState(null);


  
  const toggleModal = () => {
    setFirstModalOpen((prev) => !prev);
    if (firstModalOpen) setCurrentProduct({});
  };

  const fonnClick = (event) => {
    // Перевіряємо, чи клік був здійснений за межами модального вікна
    if (event.currentTarget === event.target) {
      //Якщо так, то додаємо код для закриття модального вікна
      toggleModal();
    }
  };

  const handleAddToCart = () => {
    const { name, price, id, image } = currentProduct;
    setCartItems((prev) => {
      const newCartState = [...prev];
      const index = newCartState.findIndex((item) => item.id === id);
      if (index !== -1) {
        newCartState[index].count++;
        saveStateToLocalStorage(CART_LS_KEY, newCartState);
        return newCartState;
      } else {
        const newState = [{ name, price, id, image, count: 1 }, ...prev];
        saveStateToLocalStorage(CART_LS_KEY, newState);

        return newState;
      }
    });
    toggleModal();
  };
  console.log(cartItems);
  const remuveAddToCart = () => {
    const { name, price, id, image } = currentProduct;
    setCartItems((prev) => {
      console.log(prev);
      let newRemuveCartState = [...prev];
      let newArrayCart = newRemuveCartState.filter((item) => item.id !== id);
      saveStateToLocalStorage(CART_LS_KEY, newArrayCart);
      return newArrayCart;
    
    });

    toggleModal();
  };

  const getData = async () => {
    try {
      const newItem = await fetch("./products.json").then((res) => res.json());
      // console.log(newItem.data);
      setitems(newItem);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    getData();
    const favFromLs = getStateFromLocalStorage(FAV_LS_KEY);
    if (favFromLs) {
      setFavorites(favFromLs);
    }
    const cartFromLs = getStateFromLocalStorage(CART_LS_KEY);
    if (cartFromLs) {
      setCartItems(cartFromLs);
    }
  }, []);

  return (
    <div className={styles.root}>
      <div className={styles.container}>
        <Header cartItems={cartItems} favorites={favorites} />
      </div>
      <AppRoutes
        remuveAddToCart={remuveAddToCart}
        cartItems={cartItems}
        favorites={favorites}
        handleAddToCart={handleAddToCart}
        currentProduct={currentProduct}
        firstModalOpen={firstModalOpen}
        fonnClick={fonnClick}
        items={items}
        setFavorites={setFavorites}
        setCurrentProduct={setCurrentProduct}
        toggleModal={toggleModal}
        setCartItems={setCartItems}
        setBtnTitle={setBtnTitle}
       
      />

    </div>
  );
}

export default App;
