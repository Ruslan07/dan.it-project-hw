import React from "react";
import { Route, Routes } from "react-router-dom";
import CartPage from "./Pages/CartPage/CartPage";
import FavoritsPage from "./Pages/FavoritsPage/FavoritsPage";
import HomePage from "./Pages/HomePage/HomePage";

const AppRoutes = ({
  remuveAddToCart,
  cartItems,
  setCartItems,

  favorites,
  handleAddToCart,
  currentProduct,
  firstModalOpen,
  fonnClick,
  items,
  setFavorites,
  setCurrentProduct,
  toggleModal,
}) => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomePage
            favorites={favorites}
            handleAddToCart={handleAddToCart}
            currentProduct={currentProduct}
            firstModalOpen={firstModalOpen}
            fonnClick={fonnClick}
            items={items}
            setFavorites={setFavorites}
            setCurrentProduct={setCurrentProduct}
            toggleModal={toggleModal}
          />
        }
      />
      <Route
        path="/cart"
        element={
          <CartPage
            cartItems={cartItems}
            remuveAddToCart={remuveAddToCart}
            toggleModal={toggleModal}
            setCartItems={setCartItems}
            firstModalOpen={firstModalOpen}
            currentProduct={currentProduct}
            fonnClick={fonnClick}
            setCurrentProduct={setCurrentProduct}
          />
        }
      />
      <Route
        path="/favorits"
        element={
          <FavoritsPage
            favorites={favorites}
            setFavorites={setFavorites}
            setCurrentProduct={setCurrentProduct}
            firstModalOpen={firstModalOpen}
            handleAddToCart={handleAddToCart}
            currentProduct={currentProduct}
            fonnClick={fonnClick}
            toggleModal={toggleModal}
          />
        }
      />
    </Routes>
  );
};
export default AppRoutes;
