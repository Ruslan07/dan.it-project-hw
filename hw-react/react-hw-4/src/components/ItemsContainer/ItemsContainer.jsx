import React from "react";
import Item from "../Item/Item";

import styles from "./ItemsContainer.module.scss";
import { useSelector, shallowEqual } from "react-redux";

const ItemsContainer = () => {
  const cartCards = useSelector((state) => state.cards.cards, shallowEqual);

  return (
    <div className="container">
      <div className={styles.productList}>
        {cartCards &&
          cartCards.map(({ id, name, price, image, setBtnTitle }) => (
            <Item
              key={id}
              name={name}
              price={price}
              image={image}
              id={id}
              setBtnTitle="Додати до кошика"
            />
          ))}
      </div>
    </div>
  );
};

export default ItemsContainer;
