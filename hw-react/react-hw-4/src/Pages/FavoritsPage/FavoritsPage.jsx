import React from "react";

import Item from "../../components/Item/Item";
import styles from "../../components/ItemsContainer/ItemsContainer.module.scss";

import { useSelector, shallowEqual } from "react-redux";

const FavoritsPage = () => {
  const favorites = useSelector((state) => state.favourite.cards, shallowEqual);
  const firstModalOpen = useSelector((state) => state.modal.isActive);
  return (
    <div className="container">
      <div className={styles.productList}>
        {favorites &&
          favorites.map(({ id, name, price, image }) => (
            <Item key={id} name={name} price={price} image={image} id={id} />
          ))}
      </div>
    </div>
  );
};
export default FavoritsPage;
