import React from "react";

import styles from "../../components/ItemsContainer/ItemsContainer.module.scss";

import { useSelector, shallowEqual } from "react-redux";

import CartItem from "../../components/CartItem/CartItem";
import FormBuy from "../../components/forms/FormBuy/FormBuy";

const CartPage = ({}) => {
  const cartCards = useSelector((state) => state.cart.cards, shallowEqual);

  return (
    <div className="container">
      <div className={styles.productList}>
        {cartCards &&
          cartCards.map(({ id, name, price, image, count }) => (
            <CartItem
              key={id}
              name={name}
              price={price}
              image={image}
              id={id}
              count={count}
            />
          ))}
         
      </div>
      <FormBuy/>
      
    </div>
  );
};
export default CartPage;
