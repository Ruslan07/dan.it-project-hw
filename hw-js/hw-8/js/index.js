/*## Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
 DOM – об'єктна модель документа, Що показує зміст html у вигляді обєктів дозволяє до них приміняти властивості динамічно .

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

За допомогою innerHTML можна очищати ,змінювати сам HTML (текст)на сторінці 
innerText ддозволяє змінювати лише текст

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

Звернутися до елемента сторінки можна : через його клас, ідентифікатор, тег, атрибут name.

Найбільш поширеним є getElementById()  querySelector() (querySelectorAll()  

*/




// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const parahraf = document.querySelectorAll('p');
parahraf.forEach((elem)=>{elem.style.background ='#ff0000'  ;

})

// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. 
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const id = document.getElementById('optionsList')
console.log(id.parentElement)

console.log(id.children);
const nod = id.childNodes
console.log(nod);



/*
Якщо я правильно зрозумів )
Властивість nodeType надає ще один, “старомодний” спосіб отримати “тип” DOM вузла.

Він має числове значення:

elem.nodeType == 1 для вузлів-елементів,
elem.nodeType == 3 для текстових вузлів,
elem.nodeType == 9 для об’єкта документа,


*/

for(let elem of nod){
   console.log(`${elem.nodeType} - ${elem.nodeName}` )
   
}

// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>
const text = document.querySelector("#testParagraph")
text.innerHTML="<p>This is a paragraph</p>"
console.log(text)



// 4) Отримати елементи `<li>` вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const li = document.querySelectorAll('.main-header li')
li.forEach((elem)=>{
  elem.className = 'nav-item' 
})
console.log(li) 




// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.  

const section = document.querySelectorAll('.section-title');
section.forEach((elem)=>{
    elem.classList.remove('section-title')
})

console.log(section)




