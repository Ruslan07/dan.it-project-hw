// 1)Описати своїми словами навіщо потрібні функції у програмуванні.

// Функції потрібні для того що спрстити написання коду ми створюємо функцію з якимось кодом
// і можимо використовувати її повторно там де нам це потрібно(викликати функцію)

// 2)Описати своїми словами, навіщо у функцію передавати аргумент.

// Аргумент – це значення, передане в функцію під час її виклику (використовується під час виконання функції)
// Аргументи передають для того щоб змінити значення у функції.Без аргументів  функція буде Nan

// 3)Що таке оператор return та як він працює всередині функції?

//  return використовується для повернення певного значення з функції до викликаючої функції.
//  Оператор return має бути останнім у функції, оскільки код після  return буде недоступним.



//                                                            Завдання

// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну -
//операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.



let sign;


do (
    sign = prompt('Виберіть дії клькулятора : adding(+) , subtraction(-), multiplication(*), dividing(/) ')
)
while (sign !== 'adding' && sign !== 'subtraction' && sign !== 'multiplication' && sign !== 'dividing')



const validateNumber = number => !(number === null || number === "" || isNaN(number));

let getNumber = (message = "Enter your number") => {

    let userNumber;

    do {
        userNumber = prompt(message);
    } while (!validateNumber(userNumber))

    return +userNumber;

}


const addingNumber = (a, b) => a + b;
const subtractionNumber = (a, b) => a - b;
const multiplicationNumber = (a, b) => a * b;
const dividingNumber = (a, b) => a / b;



const calcNumber = (calc) => {


    if (calc === 'adding') {
        const a = getNumber('Уведіть перше число');
        const b = getNumber('Уведіть друге число');
        return addingNumber(a, b);
    }

    if (calc === 'subtraction') {
        const a = getNumber('Уведіть перше число');
        const b = getNumber('Уведіть друге число');
        return subtractionNumber(a, b)
    }

    if (calc === 'multiplication') {
        const a = getNumber('Уведіть перше число');
        const b = getNumber('Уведіть друге число');
        return multiplicationNumber(a, b)
    }

    if (calc === 'dividing') {
        const a = getNumber('Уведіть перше число');
        const b = getNumber('Уведіть друге число');
        return dividingNumber(a, b)
    }

}

console.log(calcNumber(sign));