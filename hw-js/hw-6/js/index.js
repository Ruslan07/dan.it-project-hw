 /*                                               Теоретичні питання
1)Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
Використовуються спеціальні системні символи для того щоб показати програмі що значення після цього символу
застосовується як частина рядка ,а не частина коду системи(\)


2)Які засоби оголошення функцій ви знаєте?
Function Declaration,Function Expression, Стрілочна функція


3)Що таке hoisting, як він працює для змінних та функцій?
hoisting, підняття - працює якщо функція оголошена Function Declaration .
Отримує тоступ до функції,змінних то того як вона була написанна.



                                        Завдання
Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

*/


const createNewUser = () => {

    const firstName = prompt('Enter your name')
    const lastName = prompt('Enter your last name')
    const birthday = prompt('Enter your date of birth ,dd.mm.yyyy')
    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return this.firstName.slice(0, 1).toLowerCase() + this.lastName.toLowerCase()
        },
        getAge() {
            return new Date().getFullYear() - this.birthday.slice(6)
        },
        getPassword() {
            return this.firstName.slice(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.getAge()
        }
    };
    return newUser;

};

const firstNewUser = createNewUser();

console.log(firstNewUser);
console.log(firstNewUser.getLogin());
console.log(firstNewUser.getAge());
console.log(firstNewUser.getPassword());
