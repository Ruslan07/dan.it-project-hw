
const sectionsObj = {
    all1: {

        name: 'all1',
        datasetVealu : 'all1',
        img: [
            'https://fastly.picsum.photos/id/1063/285/206.jpg?hmac=kvPwfCMfYOtkItABWZZeCmR8B5yb0NAn1Xig6N38URM',
            'https://fastly.picsum.photos/id/1054/285/206.jpg?hmac=hQOSHmhC3Ykm9SS0v6mlXDZBNPk2rHSK-14RhqdUh3k',
            'https://fastly.picsum.photos/id/871/285/206.jpg?hmac=nYnv-NYz7g8_x4zN07vO1cZMpvOk3pCk_nwM62OyhWg',
            'https://fastly.picsum.photos/id/265/285/206.jpg?hmac=5dqT9G0lRguQhuI-wliuIH6uMyXLBz_APTBKAauWdn0',
            'https://fastly.picsum.photos/id/976/285/206.jpg?hmac=3cFP2IfWGwJkkgp466WOiTyfX0irLyqodpYQv_vo4NA',
            'https://fastly.picsum.photos/id/629/285/206.jpg?hmac=ikCACWeuLXcbn0sSjdlYhnXEMV1hATXgdduOwQXS_RA',
            'https://fastly.picsum.photos/id/300/285/206.jpg?hmac=OTSaWwa9bLhRQYV7dJSH9sqCADrVZ-OW2qO5hse9kFw',
            'https://fastly.picsum.photos/id/825/285/206.jpg?hmac=2Tqx52VlTPSvwpLyLr8nmfDeZrNc7gVlzO3S3ouVLNE',
            'https://fastly.picsum.photos/id/300/285/206.jpg?hmac=OTSaWwa9bLhRQYV7dJSH9sqCADrVZ-OW2qO5hse9kFw',
            'https://fastly.picsum.photos/id/825/285/206.jpg?hmac=2Tqx52VlTPSvwpLyLr8nmfDeZrNc7gVlzO3S3ouVLNE',
            'https://fastly.picsum.photos/id/941/285/206.jpg?hmac=p-ladRTiPQFDhcnkg3-R2Gvl0neuZy17AoRQrVSKmSI',
            'https://fastly.picsum.photos/id/178/285/206.jpg?hmac=_avIU1w87On6SHAETxNQeUsO_6Qw7t_0fJlu6JFtN_c',



        ],



    },
    graphicDesign2: {
        name: 'Graphic Design',
        datasetVealu : 'graphicDesign2',
        img: [
            'https://fastly.picsum.photos/id/1063/285/206.jpg?hmac=kvPwfCMfYOtkItABWZZeCmR8B5yb0NAn1Xig6N38URM',
            'https://fastly.picsum.photos/id/1054/285/206.jpg?hmac=hQOSHmhC3Ykm9SS0v6mlXDZBNPk2rHSK-14RhqdUh3k',
            'https://fastly.picsum.photos/id/871/285/206.jpg?hmac=nYnv-NYz7g8_x4zN07vO1cZMpvOk3pCk_nwM62OyhWg',
            'https://fastly.picsum.photos/id/265/285/206.jpg?hmac=5dqT9G0lRguQhuI-wliuIH6uMyXLBz_APTBKAauWdn0',
            'https://fastly.picsum.photos/id/976/285/206.jpg?hmac=3cFP2IfWGwJkkgp466WOiTyfX0irLyqodpYQv_vo4NA',
            'https://fastly.picsum.photos/id/629/285/206.jpg?hmac=ikCACWeuLXcbn0sSjdlYhnXEMV1hATXgdduOwQXS_RA',
            'https://fastly.picsum.photos/id/300/285/206.jpg?hmac=OTSaWwa9bLhRQYV7dJSH9sqCADrVZ-OW2qO5hse9kFw',
            'https://fastly.picsum.photos/id/825/285/206.jpg?hmac=2Tqx52VlTPSvwpLyLr8nmfDeZrNc7gVlzO3S3ouVLNE',



        ],
    },
    webDesign: {
        name: 'Web Design',
        datasetVealu : 'webDesign',
        img: [
            'https://fastly.picsum.photos/id/629/285/206.jpg?hmac=ikCACWeuLXcbn0sSjdlYhnXEMV1hATXgdduOwQXS_RA',
            'https://fastly.picsum.photos/id/300/285/206.jpg?hmac=OTSaWwa9bLhRQYV7dJSH9sqCADrVZ-OW2qO5hse9kFw',
            'https://fastly.picsum.photos/id/825/285/206.jpg?hmac=2Tqx52VlTPSvwpLyLr8nmfDeZrNc7gVlzO3S3ouVLNE',
            'https://fastly.picsum.photos/id/941/285/206.jpg?hmac=p-ladRTiPQFDhcnkg3-R2Gvl0neuZy17AoRQrVSKmSI',
            'https://fastly.picsum.photos/id/178/285/206.jpg?hmac=_avIU1w87On6SHAETxNQeUsO_6Qw7t_0fJlu6JFtN_c',
            'https://fastly.picsum.photos/id/974/285/206.jpg?hmac=QiYl_H0v9bh55l5g2duBztWw_rx1X7DV03AOf06pm_Y',
            'https://fastly.picsum.photos/id/139/285/206.jpg?hmac=Wdi0ve3S3j042PfBYYcd0YU0Vb-2_E_m0lrZcNNYXBs',
            'https://fastly.picsum.photos/id/813/285/206.jpg?hmac=JONV494kKLuqxxMFZmTMPI5yxjyXtWxBKFBI5Jvjw98',
        ],
    },
    landingPages: {
        name: 'Landing Pages',
        datasetVealu : 'landingPages',
        img: [
            'https://fastly.picsum.photos/id/265/285/206.jpg?hmac=5dqT9G0lRguQhuI-wliuIH6uMyXLBz_APTBKAauWdn0',
            'https://fastly.picsum.photos/id/976/285/206.jpg?hmac=3cFP2IfWGwJkkgp466WOiTyfX0irLyqodpYQv_vo4NA',
            'https://fastly.picsum.photos/id/629/285/206.jpg?hmac=ikCACWeuLXcbn0sSjdlYhnXEMV1hATXgdduOwQXS_RA',
            'https://fastly.picsum.photos/id/178/285/206.jpg?hmac=_avIU1w87On6SHAETxNQeUsO_6Qw7t_0fJlu6JFtN_c',
            'https://fastly.picsum.photos/id/974/285/206.jpg?hmac=QiYl_H0v9bh55l5g2duBztWw_rx1X7DV03AOf06pm_Y',
            'https://fastly.picsum.photos/id/139/285/206.jpg?hmac=Wdi0ve3S3j042PfBYYcd0YU0Vb-2_E_m0lrZcNNYXBs',
            'https://fastly.picsum.photos/id/300/285/206.jpg?hmac=OTSaWwa9bLhRQYV7dJSH9sqCADrVZ-OW2qO5hse9kFw',
            'https://fastly.picsum.photos/id/825/285/206.jpg?hmac=2Tqx52VlTPSvwpLyLr8nmfDeZrNc7gVlzO3S3ouVLNE',],

    },
    wordpress: {
        name: 'Wordpress',
        datasetVealu : 'wordpress',
        img: [
            'https://fastly.picsum.photos/id/178/285/206.jpg?hmac=_avIU1w87On6SHAETxNQeUsO_6Qw7t_0fJlu6JFtN_c',
            'https://fastly.picsum.photos/id/974/285/206.jpg?hmac=QiYl_H0v9bh55l5g2duBztWw_rx1X7DV03AOf06pm_Y',
            'https://fastly.picsum.photos/id/139/285/206.jpg?hmac=Wdi0ve3S3j042PfBYYcd0YU0Vb-2_E_m0lrZcNNYXBs',
            'https://fastly.picsum.photos/id/813/285/206.jpg?hmac=JONV494kKLuqxxMFZmTMPI5yxjyXtWxBKFBI5Jvjw98',
            'https://fastly.picsum.photos/id/1054/285/206.jpg?hmac=hQOSHmhC3Ykm9SS0v6mlXDZBNPk2rHSK-14RhqdUh3k',
            'https://fastly.picsum.photos/id/871/285/206.jpg?hmac=nYnv-NYz7g8_x4zN07vO1cZMpvOk3pCk_nwM62OyhWg',
            'https://fastly.picsum.photos/id/265/285/206.jpg?hmac=5dqT9G0lRguQhuI-wliuIH6uMyXLBz_APTBKAauWdn0',
            'https://fastly.picsum.photos/id/976/285/206.jpg?hmac=3cFP2IfWGwJkkgp466WOiTyfX0irLyqodpYQv_vo4NA ',
        ],

    },

}


const imgWordpress = [
    'https://fastly.picsum.photos/id/653/285/206.jpg?hmac=W_d_nR9Ve_xz_lFr4IYVCV7dM4kmYm5LAAmNNpuwtPs',
    'https://fastly.picsum.photos/id/1077/285/206.jpg?hmac=wFzVD2r5XvKCWOW8XOseh8TQrSx1bZVxMEx_XsGS3DE',
    'https://fastly.picsum.photos/id/81/285/206.jpg?hmac=mgW_ZGY8rCKKLg2WczCjFaAjlSDj5YixK9UZJo-54-E',
    'https://fastly.picsum.photos/id/941/285/206.jpg?hmac=p-ladRTiPQFDhcnkg3-R2Gvl0neuZy17AoRQrVSKmSI',
    'https://fastly.picsum.photos/id/813/285/206.jpg?hmac=JONV494kKLuqxxMFZmTMPI5yxjyXtWxBKFBI5Jvjw98',
    'https://fastly.picsum.photos/id/1063/285/206.jpg?hmac=kvPwfCMfYOtkItABWZZeCmR8B5yb0NAn1Xig6N38URM',
    'https://fastly.picsum.photos/id/84/285/206.jpg?hmac=M6dM2m7uwktP3EHrqSCd08i3LNU80xhpBZM35J3ubEY',
    'https://fastly.picsum.photos/id/122/285/206.jpg?hmac=aes2OVVJ3wAJ5Zcxq02GhDqXMfzgTO15bgh9YbKKxLc',
    'https://fastly.picsum.photos/id/519/285/206.jpg?hmac=FA6FH5whoWkVA83_A1mhtB-1WU0Pwa0wlYQ4RZOqyNs',
    'https://fastly.picsum.photos/id/80/285/206.jpg?hmac=1E47QDHvb24V207jd097TZnViMEAQxPPGMHNHraYZlI',
    'https://fastly.picsum.photos/id/15/285/206.jpg?hmac=-Rxt5u8wGIdcPbOvHMDVGW8ra_Ira73Yxj-iMmVvm_8',
    'https://fastly.picsum.photos/id/3/285/206.jpg?hmac=etI4Cci_ZzFKi81e9SCv0EHZGlaJ0gjTRdllpqCe7KA',
    'https://fastly.picsum.photos/id/941/285/206.jpg?hmac=p-ladRTiPQFDhcnkg3-R2Gvl0neuZy17AoRQrVSKmSI',
    'https://fastly.picsum.photos/id/653/285/206.jpg?hmac=W_d_nR9Ve_xz_lFr4IYVCV7dM4kmYm5LAAmNNpuwtPs',
    'https://fastly.picsum.photos/id/1077/285/206.jpg?hmac=wFzVD2r5XvKCWOW8XOseh8TQrSx1bZVxMEx_XsGS3DE',
    'https://fastly.picsum.photos/id/81/285/206.jpg?hmac=mgW_ZGY8rCKKLg2WczCjFaAjlSDj5YixK9UZJo-54-E',
    'https://fastly.picsum.photos/id/813/285/206.jpg?hmac=JONV494kKLuqxxMFZmTMPI5yxjyXtWxBKFBI5Jvjw98',
    'https://fastly.picsum.photos/id/1063/285/206.jpg?hmac=kvPwfCMfYOtkItABWZZeCmR8B5yb0NAn1Xig6N38URM',
    'https://fastly.picsum.photos/id/1054/285/206.jpg?hmac=hQOSHmhC3Ykm9SS0v6mlXDZBNPk2rHSK-14RhqdUh3k',
    'https://fastly.picsum.photos/id/871/285/206.jpg?hmac=nYnv-NYz7g8_x4zN07vO1cZMpvOk3pCk_nwM62OyhWg',
    'https://fastly.picsum.photos/id/1063/285/206.jpg?hmac=kvPwfCMfYOtkItABWZZeCmR8B5yb0NAn1Xig6N38URM',
    'https://fastly.picsum.photos/id/84/285/206.jpg?hmac=M6dM2m7uwktP3EHrqSCd08i3LNU80xhpBZM35J3ubEY',
    'https://fastly.picsum.photos/id/122/285/206.jpg?hmac=aes2OVVJ3wAJ5Zcxq02GhDqXMfzgTO15bgh9YbKKxLc',
    'https://fastly.picsum.photos/id/519/285/206.jpg?hmac=FA6FH5whoWkVA83_A1mhtB-1WU0Pwa0wlYQ4RZOqyNs',
    'https://fastly.picsum.photos/id/80/285/206.jpg?hmac=1E47QDHvb24V207jd097TZnViMEAQxPPGMHNHraYZlI',
    'https://fastly.picsum.photos/id/15/285/206.jpg?hmac=-Rxt5u8wGIdcPbOvHMDVGW8ra_Ira73Yxj-iMmVvm_8',
    'https://fastly.picsum.photos/id/3/285/206.jpg?hmac=etI4Cci_ZzFKi81e9SCv0EHZGlaJ0gjTRdllpqCe7KA',

]


// --------------------script OUR SERVICES---------------------------------



const tabsbtn = document.querySelectorAll('.tabs-title')
const tabsText = document.querySelectorAll('.tabs-content-img')
const trigl = document.querySelectorAll('.trigl');
tabsbtn.forEach((item) => {
    // console.log(item)
    item.addEventListener('click', () => {

        let btn = item;

        // console.log(btn)
        let dataTab = btn.getAttribute('data-tab')
        // console.log(dataTab)
        let dataText = document.querySelector(dataTab)
        // console.log(dataText);
        let dataTrigl = btn.getAttribute('data-trigl')
        // console.log(dataTrigl)
        let triglNew = document.querySelector(dataTrigl)
        // console.log(triglNew);


        if (!dataText.classList.contains('active')) {

            tabsbtn.forEach((item) => {
                item.classList.remove('active')
            })
            tabsText.forEach((item) => {
                item.classList.remove('active')
            })
            trigl.forEach((item) => {
                item.classList.remove('active')
            })

            btn.classList.add('active')
            dataText.classList.add('active')
            triglNew.classList.add('active')
        }

    })
})


// ----------------------script Our Amazing Work------------------////////
// Цю секцію у мені не вдалося зробити нормально,зображення добавляються тільки при кліку тому я добавив їх в html(







const tabsBtnProfessional = document.querySelectorAll('.main-text-li')
console.log(tabsBtnProfessional);
let tabsImg = document.querySelector('.profession-img')
console.log(tabsImg)


tabsBtnProfessional.forEach((item) => {

    // console.log(item)
    item.addEventListener('click', (e) => {
        
        let btn = item;
        // console.log(tabsImg)
        let dataTab = btn.getAttribute('data-tab')
        // console.log(dataTab);

        tabsBtnProfessional.forEach((e) => {
            // console.log(e)
            e.classList.remove('active')
        })
        for (let key of tabsImg.children) {
            // console.log(key.classList.contains('active2'))
            if (!key.classList.contains('active2')) {

            }
            key.style.display = 'none'

        }
        btn.classList.add('active')
        tabsImg.style.display = 'flex'

        if (sectionsObj[dataTab]) {

            const obj = sectionsObj[dataTab];
            // console.log(obj)
            obj.img.forEach((url) => {
                tabsImg.insertAdjacentHTML('afterbegin',
                    `<div class="container-img-deseign active2">
    <div class='flip-card'>
       <div class="img-front">
          <img src="${url}" alt="${obj.name}">
       </div>
       <div class="img-back">
          <div class='img-card-back'>
                <div class="img-card-circle-1">
                   <img class="svg-position" src="./img/svg/img-card-circle-1.svg" alt="">
                </div>

            <div class="img-card-circle-2">
                <div class="img-card-circle-2-square svg-position "></div>
            </div>
            </div>
           <p class="text text-card-back-p ">creative design</p>
           <p class="text card-back-2p">${obj.name}</p>
        </div>
    </div>
</div>`)
            })

        }


    }
    )

});



// const tabsBtnProfessional = document.querySelectorAll('.main-text-li')


//         let tabsImg = document.querySelector('.profession-img')


//         const foto =(imges) => {

       
//         for (let key in imges) {
//             // console.log(key)
//             // if (key === dataTab) {
//                 const obj = imges[key];
//                 // console.log(obj)
//                 obj.img.forEach((url) => {
                    
//                     tabsImg.insertAdjacentHTML('afterbegin',
//                         `<div class="container-img-deseign  " id="${obj.datasetVealu}">
//         <div class='flip-card'>
//            <div class="img-front">
//               <img src="${url}" alt="${obj.name}">
//            </div>
//            <div class="img-back">
//               <div class='img-card-back'>
//                     <div class="img-card-circle-1">
//                        <img class="svg-position" src="./img/svg/img-card-circle-1.svg" alt="">
//                     </div>
    
//                 <div class="img-card-circle-2">
//                     <div class="img-card-circle-2-square svg-position "></div>
//                 </div>
//                 </div>
//                <p class="text text-card-back-p ">creative design</p>
//                <p class="text card-back-2p">${obj.name}</p>
//             </div>
//         </div>
//     </div>`)
//                 })
    
//             }
//         }
//         foto(sectionsObj)
         


// // -----------------------script button load more------------------// //




const loadBtn = document.querySelector('.load-btn');

const load = document.querySelector('#preloader');
let guantity = 0;
const professionImg = document.querySelector('.profession-img');
// console.log(professionImg)



const professionImgHtml = imgWordpress.map((elem) => `
 <div class="container-img-deseign">
    <div class='flip-card'>
       <div class="img-front">
          <img src="${elem}" alt="">
       </div>
       <div class="img-back">
          <div class='img-card-back'>
                <div class="img-card-circle-1">
                   <svg class="svg-position" width="15" height="15" viewBox="0 0 15 15" fill="#1FDAB5" xmlns="http://www.w3.org/2000/svg">
                   <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" />

</style>  </svg >
                </div>

            <div class="img-card-circle-2">
                <div class="img-card-circle-2-square svg-position "></div>
            </div>
            </div>
           <p class="text text-card-back-p ">creative design</p>
           <p class="text card-back-2p">All</p>
        </div>
    </div>
</div>`)

// console.log(professionImgHtml);

professionImg.insertAdjacentHTML('beforeend', professionImgHtml.slice(0, guantity).join(''))


loadBtn.addEventListener('click', (e) => {
    // console.log(e)
    load.classList.toggle('preloader-active')
    loadBtn.style.displey = 'none';


    setTimeout(() => {
        load.classList.toggle('preloader-active')
        loadBtn.style.displey = 'block';
        professionImg.insertAdjacentHTML('beforeend', professionImgHtml.slice(guantity, guantity + 12).join(''));
        guantity = guantity + 12;
        if (imgWordpress.length <= guantity) {
            loadBtn.style.display = 'none';
        }

    }, 2000)

})










// -----------------------------script slide----------------------

const usersFoto = document.querySelector('.users-foto');
// console.log(usersFoto.target)

const clickFoto = (event) => {
    if (event.target.tagName === 'IMG') {
        let img = event.target
        const userHistory = document.querySelectorAll('.user-history')
        let userImgActive = document.querySelector('.user-img.active')
        console.log(userImgActive)
        userImgActive.classList.remove('active');
        img.classList.add('active');
        userHistory[0].classList.add('none');

        for (let elem of userHistory) {

            img.dataset.user !== elem.dataset.user ? elem.classList.replace('move', 'none') : elem.classList.add('move');
            // console.log(elem)
        }

    }

}


usersFoto.addEventListener('click', clickFoto)









const btnLeft = document.querySelector('.btn-carousel.arrow-left')
// console.log(btnLeft)
const btnRight = document.querySelector('.btn-carousel.arrow-right')



const clickButtonLeft = () => {
    const userHistory = document.querySelectorAll('.user-history')
    const userImgs = document.querySelectorAll('.user-img')
    const userImgActive = document.querySelector('.user-img.active')
    const userImgActiveIndex = ([...userImgs].indexOf(userImgActive));
    userImgActive.classList.remove('active')
    let activeNev;
    if (userImgActiveIndex === 0) {
        activeNev = userImgs[userImgs.length - 1]
        console.log(userImgs[userImgs.length - 1])
        userImgs[userImgs.length - 1].classList.add('active')

    } else {
        activeNev = userImgs[userImgActiveIndex - 1]
        userImgs[userImgActiveIndex - 1].classList.add('active')
    }
    userHistory[0].classList.add('none')
    for (let elem of userHistory) {

        activeNev.dataset.user !== elem.dataset.user ? elem.classList.replace('move', 'none') : elem.classList.add('move');
        // console.log(elem)
    }
}



const clickButtonRight = () => {
    const userHistory = document.querySelectorAll('.user-history')
    const userImgs = document.querySelectorAll('.user-img')
    const userImgActive = document.querySelector('.user-img.active')
    const userImgActiveIndex = ([...userImgs].indexOf(userImgActive));
    userImgActive.classList.remove('active')
    let activeNev;
    if (userImgActiveIndex === userImgs.length - 1) {
        activeNev = userImgs[0]
        userImgs[0].classList.add('active')

    } else {
        activeNev = userImgs[userImgActiveIndex + 1]
        userImgs[userImgActiveIndex + 1].classList.add('active')
    }
    userHistory[0].classList.add('none')
    for (let elem of userHistory) {

        activeNev.dataset.user !== elem.dataset.user ? elem.classList.replace('move', 'none') : elem.classList.add('move');
        // console.log(elem)
    }
}
btnLeft.addEventListener('click', clickButtonLeft)
btnRight.addEventListener('click', clickButtonRight)











