// ## Теоретичні питання

// 1. Опишіть, як можна створити новий HTML тег на сторінці.
//  За допомогою document.createElement(tagNam) - створює обєктне уявлення DOM елемента , де tagNam це імя тегу



// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

// elem.insinsertAdjacentHTML(place, HTMLstring)

// place приймає одне з чотирьох значень:

// beforeend - вставити html в кінець elem,
// afterbegin – вставити html на початок elem,
// beforebegin – вставити html безпосередньо перед elem (сестринські),
// afterend – вставити html безпосередньо після elem (сестринські).




// 3. Як можна видалити елемент зі сторінки?
// За допомогою методу remove()

// ## Завдання

// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.
//  Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент,
//  до якого буде прикріплений список (по дефолту має бути document.body.
// - кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// Приклади масивів, які можна виводити на екран:

// ```javascript
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ```

// ```javascript
// ["1", "2", "3", "sea", "user", 23];
// ```

// - Можна взяти будь-який інший масив.

const contry = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", 777, 909090];



const list = (array, parent = document.body) => {
   const ol = document.createElement('ol')

   array.forEach(element => {
      const li = document.createElement('li')
      li.innerText = element;

      ol.append(li)

   });
   parent.append(ol)

}
list(contry);

setTimeout(function () { document.body.innerHTML = ''; }, 3000);








