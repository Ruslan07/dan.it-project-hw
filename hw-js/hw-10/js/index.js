// ## Завдання

// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// #### Технічні вимоги:
// - У папці `tabs` лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався
//  конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено,
//  який текст має відображатися для якої вкладки.
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// - Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. 
// При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

// #### Література:
// - [Використання data-* атрибутів](https://developer.mozilla.org/ru/docs/Learn/HTML/Howto/Use_data_attributes)

// Зробив два варіанти , але перший не вийшло зробити універсальним 
//  при видаленні елемента код ламаїїця не зміг розібратися чому?(

// варіант № 1

// const tabs = document.querySelector('.tabs');

// tabs.addEventListener('click', (event) => {

//   const btn = event.target.dataset.tab;

//   const dataText = document.querySelector(btn)
 

//   const tabsContent = document.querySelector('.active')
//   tabsContent.classList.remove('active');
//   const activBtn = document.querySelector('.active')
//   activBtn.classList.remove('active');

//   dataText.classList.add('active')
//   event.target.classList.add('active')


// })

// варіант № 2

const tabsbtn = document.querySelectorAll('.tabs-title')
const tabsText = document.querySelectorAll('.text')

tabsbtn.forEach((item) => {
  item.addEventListener('click', () => {

    let btn = item;
    let dataTab = btn.getAttribute('data-tab');
    console.log(dataTab);
    let dataText = document.querySelector(dataTab)
if( !dataText.classList.contains('active')){
  //  console.log(dataText);
    tabsbtn.forEach((item) => {
      item.classList.remove('active')
    })

    tabsText.forEach((item) => {
      item.classList.remove('active')
    })

    btn.classList.add('active')
    dataText.classList.add('active')
}


  })
})




