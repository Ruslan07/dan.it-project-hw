/*

## Завдання

Реалізувати можливість зміни колірної теми користувача.Завдання має бути 
виконане на чистому Javascript без використання бібліотек типу jQuery або React.

#### Технічні вимоги:
- Взяти будь-яке готове домашнє завдання з HTML/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо)
 на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для 
 сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки


*/

const btn = document.createElement('button')

btn.innerText ='Змінити тему';
 const div = document.querySelector('.top')
btn.style = " background-color:#E4B564;cursor: pointer;"
div.append(btn)
let header = document.querySelector('.container-header')

 btn.addEventListener('click',()=>{
    
    const story = header.classList.toggle('background')
    console.log(story);
    localStorage.setItem('fons',story)
    console.log(localStorage.getItem('fons'))

})

if(localStorage.getItem('fons') === 'true'){
    header.classList.add('background')
}



